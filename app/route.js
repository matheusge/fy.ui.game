'use strict';

var config = function Config($routeProvider) {

    $routeProvider

        //Start Screen
        .when('/', {
            templateUrl: 'app/template/start.html',
            controller: 'StartCtrl'
        })

        //CPF Input
        .when('/cpf-input', {
            templateUrl: 'app/template/cpf-input.html',
            controller: 'ClienteCtrl'
        })

        //Pontos
        .when('/pontos', {
            templateUrl: 'app/template/pontos.html',
            controller: 'PontosCtrl'
        })

        //Cadastro de Clientes
        .when('/cad-cliente/:cpf', {
            // url: 'cad-cliente/{cpf}' ,
            templateUrl: 'app/template/cad-cliente.html',
            controller: 'CadClienteCtrl'
        })

    .otherwise({ redirectTo: '/' });
};

angular
    .module('fy')
    .config(config);
    