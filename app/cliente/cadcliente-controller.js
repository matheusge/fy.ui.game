'use strict';

var cadClienteController = function CadClienteController($scope, $location, $routeParams, ClienteAPI){
    angular.element('body').attr('name', 'body-cadcliente');
    
    var new_cpf = $routeParams.cpf;

    $scope.createCliente = function(n, c, e){
            ClienteAPI.createCliente(new_cpf, n, c, e).success(function(data){
                if(data.resposta == "OK"){
                    $location.path('/pontos');
                }else{
                    // var url = '/cad-cliente/' + value
                    // $location.path(url);
                    console.log('ERRO - Tratar mensagens!');
                }
            }).error(function(data, status){
                console.log("ERRO:" + status);
            });
    };
};

angular.
    module('fy')
    .controller('CadClienteCtrl', cadClienteController);