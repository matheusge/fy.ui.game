'use strict';

var factory = function($http){
    var _getAll = function(){
        return $http.get('http://localhost:8080/cliente');
    };

    var _getByCPF = function(v){
        return $http.get('http://localhost:8080/cliente/cpf/' + v);
    };

    var _getByCPF_Pontuar = function(cpf, p){
        return $http.post('http://localhost:8080/clientep', { 'data': { cpf, p } });
    };

    var _createCliente = function(c, n, cel, e){
        /*
            c = cpf;
            n = nome;
            cel = celular
            e = email
        */
        return $http.post('http://localhost:8080/cliente/create', { 'data': { c, n, cel, e }});
    }

    return {
        getAll: _getAll,
        getByCPF: _getByCPF,
        getByCPF_Pontuar: _getByCPF_Pontuar,
        createCliente: _createCliente
    };
};

angular
    .module('fy')
    .factory('ClienteAPI', factory);