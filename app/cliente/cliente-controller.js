'use strict';

var clienteController = function ClienteController($scope, $location, ClienteAPI) {
    $scope.cliente_cpf = '';
    angular.element('body').attr('name', 'body-cliente');
    $scope.findCliente = function(value){
        if(value != ""){
            ClienteAPI.getByCPF_Pontuar(value).success(function(data){
                if(data.resposta == "OK"){
                    $location.path('/pontos');
                }else{
                    var url = '/cad-cliente/' + value
                    $location.path(url);
                }
            }).error(function(data, status){
                console.log("ERRO:" + status);
            });
        }else{
            $("#cpf").focus();
            $('ol.questions').animateCss('bounce');
        }
    };
};

angular.
    module('fy')
    .controller('ClienteCtrl', clienteController);
    
