'use strict';

var startController = function StartController($scope, $location) {
    angular.element('body').attr('name', 'body-start');

    $scope.functionSubmit = function(){

        // Define ng-model in kind component in the form 
        // console.log($scope.email);
        // console.log($scope.passsord);

        $location.path('/cpf-input');
    }
}

angular.
    module('fy')
    .controller('StartCtrl', startController);
    

